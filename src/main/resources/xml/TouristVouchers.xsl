<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tableFmt {
                        border: 1px ;
                        width:100%;
                    }

                    #row td {
                        padding:10px 0 10px 0;
                        background-color: #FFE599;
                        color: black;
                        text-align:center;
                        border: 1px solid black;
                    }

                    th {
                        background-color: #2E9AFE;
                        color: white;
                    }

                </style>
            </head>

            <body>
                <table class="tableFmt">
                    <tr>
                        <th style="width:10%">Number</th>
                        <th style="width:10%">Type</th>
                        <th style="width:15%">Country</th>
                        <th style="width:10%">Duration</th>
                        <th style="width:10%">Transport</th>
                        <th style="width:20%">Hotel</th>
                        <th style="width:20%">Room</th>
                        <th style="width:5%">Cost</th>
                    </tr>

                    <xsl:for-each select="tourist_vouchers/voucher">

                        <tr id="row">
                            <td>
                                <div><xsl:value-of select="@voucher_type"/></div>
                            </td>
                            <td>
                                <div><xsl:value-of select="type" /></div>
                            </td>
                            <td>
                                <div><xsl:value-of select="country" /></div>
                            </td>
                            <td>
                                <div><xsl:value-of select="duration" /> days</div>
                            </td>
                            <td>
                                <div><xsl:value-of select="transport" /></div>
                            </td>
                            <td>
                                <div><xsl:value-of select="hotel/stars" /> stars</div>
                                <div>Food - <xsl:value-of select="hotel/food" /></div>
                            </td>
                            <td>
                                <div>Room for <xsl:value-of select="hotel/room/size"/></div>
                                <xsl:if test="hotel/room/tv='true'">
                                    <div>TV</div>
                                </xsl:if>

                                <xsl:if test="hotel/room/airCondition='true'">
                                    <div>Air Condition</div>
                                </xsl:if>

                                <xsl:if test="hotel/room/phone='true'">
                                    <div>Phone</div>
                                </xsl:if>

                                <xsl:if test="hotel/room/wifi='true'">
                                    <div>Wi-Fi</div>
                                </xsl:if>

                                <xsl:if test="hotel/room/wardrobe='true'">
                                    <div>Wardrobe</div>
                                </xsl:if>
                            </td>
                            <td>
                                <div><xsl:value-of select="cost" /> €</div>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>