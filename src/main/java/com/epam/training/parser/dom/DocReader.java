package com.epam.training.parser.dom;

import com.epam.training.comparator.VoucherComparator;
import com.epam.training.model.Hotel;
import com.epam.training.model.Room;
import com.epam.training.model.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DocReader {
    private static final Logger logger = LogManager.getLogger(DocReader.class.getName());
    private Document document;

    public DocReader(Document document) {
        this.document = document;
    }

    public void readDocAndPrintVouchers(){
        List<Voucher> voucherList = new ArrayList<>();
        NodeList vouchers = document.getDocumentElement().getElementsByTagName(
                "voucher");
        for (int i = 0; i < vouchers.getLength(); i++) {
            Node node = vouchers.item(i);
            Element element = (Element) node;
            Voucher voucher = fillVoucher(element);
            voucherList.add(voucher);
        }
        voucherList.sort(new VoucherComparator());
        voucherList.forEach(logger::info);
    }

    private Voucher fillVoucher(Element element){
        Voucher voucher = new Voucher();
        voucher.setVoucherNumber(Integer.parseInt(
                element.getAttribute("voucher_number")));
        voucher.setType(element.getElementsByTagName("type")
                .item(0).getTextContent());
        voucher.setCountry(element.getElementsByTagName("country")
                .item(0).getTextContent());
        voucher.setDuration(Integer.parseInt(
                element.getElementsByTagName("duration")
                        .item(0).getTextContent()));
        voucher.setTransport(element.getElementsByTagName("transport")
                .item(0).getTextContent());
        Hotel hotel = fillHotel(element.getElementsByTagName("hotel"));
        voucher.setHotel(hotel);
        voucher.setCost(BigDecimal.valueOf(
                Double.parseDouble(element.getElementsByTagName(
                "cost").item(0).getTextContent())));
        return voucher;
    }

    private Hotel fillHotel(NodeList nodeList){
        Hotel hotel = new Hotel();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            hotel.setStars(Integer.parseInt(element.getElementsByTagName(
                    "stars").item(0).getTextContent()));
            hotel.setFood(element.getElementsByTagName("food")
                    .item(0).getTextContent());
            Room room = fillRoom(element.getElementsByTagName("room"));
            hotel.setRoom(room);
        }
        return hotel;
    }

    private Room fillRoom(NodeList nodeList){
        Room room = new Room();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            room.setSize(Integer.parseInt(element
                    .getElementsByTagName("size")
                    .item(0).getTextContent()));
            room.setHasTV(Boolean.parseBoolean(element
                    .getElementsByTagName("tv")
                    .item(0).getTextContent()));
            room.setHasAirCondition(Boolean.parseBoolean(element
                    .getElementsByTagName("airCondition")
                    .item(0).getTextContent()));
            room.setHasPhone(Boolean.parseBoolean(element
                    .getElementsByTagName("phone")
                    .item(0).getTextContent()));
            room.setHasWifi(Boolean.parseBoolean(element
                    .getElementsByTagName("wifi")
                    .item(0).getTextContent()));
            room.setHasWardrobe(Boolean.parseBoolean(element
                    .getElementsByTagName("wardrobe")
                    .item(0).getTextContent()));
        }
        return room;
    }
}
