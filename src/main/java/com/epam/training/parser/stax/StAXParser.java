package com.epam.training.parser.stax;

import com.epam.training.comparator.VoucherComparator;
import com.epam.training.model.Hotel;
import com.epam.training.model.Room;
import com.epam.training.model.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class StAXParser {
    private static final Logger logger = LogManager.getLogger(StAXParser.class.getName());
    private Voucher voucher;
    private Hotel hotel;
    private Room room;
    private List<Voucher> vouchers;

    public StAXParser() {
        vouchers = new ArrayList<>();
    }

    public void parseAndPrint(File xml) {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    boolean found = startElementVoucher(xmlEvent, xmlEventReader);
                    if (!found)
                        found = startElementHotel(xmlEvent, xmlEventReader);
                    if (!found) startElementRoom(xmlEvent, xmlEventReader);
                }
                if (xmlEvent.isEndElement()) {
                    endElement(xmlEvent);
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        vouchers.sort(new VoucherComparator());
        vouchers.forEach(logger::info);
    }

    private void endElement(XMLEvent xmlEvent) {
        EndElement endElement = xmlEvent.asEndElement();
        String name = endElement.getName().getLocalPart();
        switch (name) {
            case "voucher": {
                vouchers.add(voucher);
                break;
            }
            case "hotel": {
                voucher.setHotel(hotel);
                break;
            }
            case "room": {
                hotel.setRoom(room);
                break;
            }
        }
    }

    private boolean startElementVoucher(XMLEvent xmlEvent, XMLEventReader xmlEventReader) {
        boolean found = false;
        try {
            StartElement startElement = xmlEvent.asStartElement();
            String name = startElement.getName().getLocalPart();
            switch (name) {
                case "voucher": {
                    found = true;
                    voucher = new Voucher();
                    Attribute voucherNumber =
                            startElement.getAttributeByName(
                                    new QName("voucher_number"));
                    if (voucherNumber != null) {
                        voucher.setVoucherNumber(Integer.parseInt(
                                voucherNumber.getValue()));
                    }
                    break;
                }
                case "type": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    voucher.setType(xmlEvent.asCharacters().getData());
                    break;
                }
                case "country": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    voucher.setCountry(xmlEvent.asCharacters().getData());
                    break;
                }
                case "duration": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    voucher.setDuration(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "transport": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    voucher.setTransport(xmlEvent.asCharacters().getData());
                    break;
                }
                case "cost": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    voucher.setCost(BigDecimal.valueOf(Double.parseDouble(xmlEvent.asCharacters().getData())));
                    break;
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return found;
    }

    private boolean startElementHotel(XMLEvent xmlEvent, XMLEventReader xmlEventReader) {
        boolean found = false;
        try {
            StartElement startElement = xmlEvent.asStartElement();
            String name = startElement.getName().getLocalPart();
            switch (name) {
                case "hotel": {
                    found = true;
                    hotel = new Hotel();
                    break;
                }
                case "stars": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    hotel.setStars(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "food": {
                    found = true;
                    xmlEvent = xmlEventReader.nextEvent();
                    hotel.setFood(xmlEvent.asCharacters().getData());
                    break;
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return found;
    }

    private void startElementRoom(XMLEvent xmlEvent, XMLEventReader xmlEventReader) {
        try {
            StartElement startElement = xmlEvent.asStartElement();
            String name = startElement.getName().getLocalPart();
            switch (name) {
                case "room": {
                    room = new Room();
                    break;
                }
                case "size": {
                    xmlEvent = xmlEventReader.nextEvent();
                    room.setSize(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "tv": {
                    xmlEvent = xmlEventReader.nextEvent();
                    room.setHasTV(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "airCondition": {
                    xmlEvent = xmlEventReader.nextEvent();
                    room.setHasAirCondition(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "wifi": {
                    xmlEvent = xmlEventReader.nextEvent();
                    room.setHasWifi(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "phone": {
                    xmlEvent = xmlEventReader.nextEvent();
                    room.setHasPhone(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    break;
                }
                case "wardrobe": {
                    xmlEvent = xmlEventReader.nextEvent();
                    room.setHasWardrobe(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    break;
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
