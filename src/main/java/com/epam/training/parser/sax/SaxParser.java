package com.epam.training.parser.sax;

import com.epam.training.comparator.VoucherComparator;
import com.epam.training.model.Voucher;
import com.epam.training.validator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SaxParser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    private static final Logger logger = LogManager.getLogger(SaxParser.class.getName());

    public static void parseAndPrint(File xml, File xsd) {
        List<Voucher> vouchers = new ArrayList<>();

        try {
            saxParserFactory.setNamespaceAware(true);
            saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
            saxParserFactory.setValidating(true);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxHandler saxHandler = new SaxHandler();
            saxParser.parse(xml, saxHandler);
            vouchers = saxHandler.getVouchers();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
        vouchers.sort(new VoucherComparator());
        vouchers.forEach(logger::info);
    }
}
