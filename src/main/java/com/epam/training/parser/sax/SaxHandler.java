package com.epam.training.parser.sax;

import com.epam.training.model.Hotel;
import com.epam.training.model.Room;
import com.epam.training.model.Voucher;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {
    private String thisElement;
    private Voucher voucher;
    private Hotel hotel;
    private Room room;
    private List<Voucher> vouchers ;

    SaxHandler() {
        vouchers = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        thisElement = qName;
        switch (thisElement) {
            case "voucher":{
                String voucherNumber = attributes.getValue("voucher_number");
                voucher = new Voucher();
                voucher.setVoucherNumber(Integer.parseInt(voucherNumber));
            } break;
            case "hotel":{
                hotel = new Hotel();
            }break;
            case "room":{
                room = new Room();
            }break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "voucher":{
                vouchers.add(voucher);
            } break;
            case "hotel":{
                voucher.setHotel(hotel);
            }break;
            case "room":{
                hotel.setRoom(room);
            }break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        setVoucher(ch, start,length);
        setHotel(ch, start,length);
        setRoom(ch, start,length);
    }

    private void setHotel(char[] ch, int start, int length){
        if (thisElement.equals("stars")) {
            hotel.setStars(parseInt(ch, start,length));
        }
        if (thisElement.equals("stars")) {
            hotel.setFood(new String(ch, start,length));
        }
    }

    private void setRoom(char[] ch, int start, int length){
        if (thisElement.equals("size")) {
            room.setSize(parseInt(ch, start,length));
        }
        if (thisElement.equals("tv")) {
            room.setHasTV(parseBoolean(ch, start,length));
        }
        if (thisElement.equals("airCondition")) {
            room.setHasAirCondition(parseBoolean(ch, start,length));
        }
        if (thisElement.equals("wifi")) {
            room.setHasWifi(parseBoolean(ch, start,length));
        }
        if (thisElement.equals("phone")) {
            room.setHasPhone(parseBoolean(ch, start,length));
        }
        if (thisElement.equals("wardrobe")) {
            room.setHasWardrobe(parseBoolean(ch, start,length));
        }
    }

    private void setVoucher(char[] ch, int start, int length){
        if (thisElement.equals("type")) {
            voucher.setType(new String(ch, start, length));
        }
        if (thisElement.equals("country")) {
            voucher.setCountry(new String(ch, start, length));
        }
        if (thisElement.equals("transport")) {
            voucher.setTransport(new String(ch, start, length));
        }
        if (thisElement.equals("duration")) {
            voucher.setDuration(parseInt(ch, start,length));
        }
        if (thisElement.equals("cost")) {
            voucher.setCost(parseBigDecimal(ch, start,length));
        }
    }

    private Boolean parseBoolean(char[] ch, int start, int length){
        return Boolean.parseBoolean(new String(ch, start,
                length));
    }

    private Integer parseInt(char[] ch, int start, int length){
        return Integer.parseInt(new String(ch, start,
                length));
    }

    private BigDecimal parseBigDecimal(char[] ch, int start, int length){
        return BigDecimal.valueOf(
                Double.parseDouble(
                        new String(ch, start, length)));
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }
}
