package com.epam.training.view;

import com.epam.training.converter.XMLToHTMLConverter;
import com.epam.training.parser.dom.DocBuilder;
import com.epam.training.parser.dom.DocReader;
import com.epam.training.parser.sax.SaxParser;
import com.epam.training.parser.stax.StAXParser;
import com.epam.training.validator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private ResourceBundle bundle;
    private File xmlFile;
    private File xsdFile;
    private File xslFile;

    public View() {
        bundle = ResourceBundle.getBundle("Menu");
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        xmlFile = new File("src\\main\\resources\\xml\\TouristVouchers" +
                ".xml");
        xsdFile = new File("src\\main\\resources\\xml\\TouristVouchers" +
                ".xsd");
        xslFile = new File("src\\main\\resources\\xml\\TouristVouchers" +
                ".xsl");
    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::readByDomParser);
        methodsMenu.put("2", this::readBySaxParser);
        methodsMenu.put("3", this::readByStaxParser);
        methodsMenu.put("4", this::convertXMLToHTML);

        outputMenu();
    }

    private void readByDomParser(){
        DocBuilder docBuilder = new DocBuilder(xmlFile);
        if (XmlValidator.validate(docBuilder.getDoc(), xsdFile)) {
            DocReader reader = new DocReader(docBuilder.getDoc());
            reader.readDocAndPrintVouchers();
        } else {
            System.out.println("XML document failed validation.");
        }
    }

    private void readBySaxParser(){
        SaxParser.parseAndPrint(xmlFile,xsdFile);
    }

    private void readByStaxParser(){
        StAXParser staxParser = new StAXParser();
        staxParser.parseAndPrint(xmlFile);
    }

    private void convertXMLToHTML(){
        new XMLToHTMLConverter(xmlFile, xslFile);
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
