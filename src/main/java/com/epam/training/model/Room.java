package com.epam.training.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Room {
    private int size;
    private boolean hasTV;
    private boolean hasAirCondition;
    private boolean hasWifi;
    private boolean hasPhone;
    private boolean hasWardrobe;

    public Room() {
    }

    public Room(int size, boolean hasTV, boolean hasAirCondition, boolean hasWifi, boolean hasPhone, boolean hasWardrobe) {
        this.size = size;
        this.hasTV = hasTV;
        this.hasAirCondition = hasAirCondition;
        this.hasWifi = hasWifi;
        this.hasPhone = hasPhone;
        this.hasWardrobe = hasWardrobe;
    }

    @Override
    public String toString() {
        return "Room{" +
                "size=" + size +
                ", hasTV=" + hasTV +
                ", hasAirCondition=" + hasAirCondition +
                ", hasWifi=" + hasWifi +
                ", hasPhone=" + hasPhone +
                ", hasWardrobe=" + hasWardrobe +
                '}';
    }
}
