package com.epam.training.converter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class XMLToHTMLConverter {
    private static final Logger logger = LogManager.getLogger(XMLToHTMLConverter.class.getName());

    public XMLToHTMLConverter(File xmlFile, File xslFile) {
        Source xml = new StreamSource(xmlFile);
        Source xsl = new StreamSource(xslFile);
        convertXMLToHTML(xml, xsl);
    }

    private void convertXMLToHTML(Source xml, Source xsl) {
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter("src\\main\\resources\\xml" +
                    "\\TouristVouchers.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(xsl);

            transformer.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            logger.info("TouristVouchers.html generated successfully.");
        } catch (IOException | TransformerFactoryConfigurationError
                | TransformerException e) {
            e.printStackTrace();
        }
    }
}
